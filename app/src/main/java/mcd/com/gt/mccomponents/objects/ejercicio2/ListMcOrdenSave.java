package mcd.com.gt.mccomponents.objects.ejercicio2;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aleksei.escobar on 27/11/2017.
 */

public class ListMcOrdenSave implements Parcelable {
    public List<McOrden> mcOrdenList;

    public ListMcOrdenSave() {
    }

    public ListMcOrdenSave(List<McOrden> mcOrdenList) {
        this.mcOrdenList = mcOrdenList;
    }

    protected ListMcOrdenSave(Parcel in) {
        if (in.readByte() == 0x01) {
            mcOrdenList = new ArrayList<McOrden>();
            in.readList(mcOrdenList, McOrden.class.getClassLoader());
        } else {
            mcOrdenList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (mcOrdenList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mcOrdenList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ListMcOrdenSave> CREATOR = new Parcelable.Creator<ListMcOrdenSave>() {
        @Override
        public ListMcOrdenSave createFromParcel(Parcel in) {
            return new ListMcOrdenSave(in);
        }

        @Override
        public ListMcOrdenSave[] newArray(int size) {
            return new ListMcOrdenSave[size];
        }
    };

    public List<McOrden> getMcOrdenList() {
        return mcOrdenList;
    }

    public void setMcOrdenList(List<McOrden> mcOrdenList) {
        this.mcOrdenList = mcOrdenList;
    }
}
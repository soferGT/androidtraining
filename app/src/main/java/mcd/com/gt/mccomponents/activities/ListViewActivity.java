package mcd.com.gt.mccomponents.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mcd.com.gt.mccomponents.R;

public class ListViewActivity extends AppCompatActivity {

    private EditText etElement;
    private ListView lvOpt;
    private ArrayAdapter adapter;
    private ArrayList<String> listItems=new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        initComponents();
    }

    private void initComponents(){
        etElement = (EditText) findViewById(R.id.et_new_element);
        String[] options = getResources().getStringArray(R.array.img_opt);
        listItems.addAll(Arrays.asList(options));
        lvOpt = (ListView) findViewById(R.id.lv_opt1);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,listItems);
        lvOpt.setAdapter(adapter);
    }

    public void addItem(View view){
        listItems.add(etElement.getText().toString());
        etElement.getText().clear();
    }
}

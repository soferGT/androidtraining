package mcd.com.gt.mccomponents.objects.ejercicio1;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aleksei.escobar on 10/10/2017.
 */

public class NuevaOrden implements Parcelable {
    private List<Hamburguesa> listHamburguesas;
    private List<McFlury> listMcFlurys;
    private List<Cafe> listCafes;

    public NuevaOrden() {
    }

    public NuevaOrden(List<Hamburguesa> listHamburguesas, List<McFlury> listMcFlurys, List<Cafe> listCafes) {
        this.listHamburguesas = listHamburguesas;
        this.listMcFlurys = listMcFlurys;
        this.listCafes = listCafes;
    }

    protected NuevaOrden(Parcel in) {
        if (in.readByte() == 0x01) {
            listHamburguesas = new ArrayList<Hamburguesa>();
            in.readList(listHamburguesas, Hamburguesa.class.getClassLoader());
        } else {
            listHamburguesas = null;
        }
        if (in.readByte() == 0x01) {
            listMcFlurys = new ArrayList<McFlury>();
            in.readList(listMcFlurys, McFlury.class.getClassLoader());
        } else {
            listMcFlurys = null;
        }
        if (in.readByte() == 0x01) {
            listCafes = new ArrayList<Cafe>();
            in.readList(listCafes, Cafe.class.getClassLoader());
        } else {
            listCafes = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (listHamburguesas == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(listHamburguesas);
        }
        if (listMcFlurys == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(listMcFlurys);
        }
        if (listCafes == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(listCafes);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<NuevaOrden> CREATOR = new Parcelable.Creator<NuevaOrden>() {
        @Override
        public NuevaOrden createFromParcel(Parcel in) {
            return new NuevaOrden(in);
        }

        @Override
        public NuevaOrden[] newArray(int size) {
            return new NuevaOrden[size];
        }
    };

    public List<Hamburguesa> getListHamburguesas() {
        return listHamburguesas;
    }

    public void setListHamburguesas(List<Hamburguesa> listHamburguesas) {
        this.listHamburguesas = listHamburguesas;
    }

    public List<McFlury> getListMcFlurys() {
        return listMcFlurys;
    }

    public void setListMcFlurys(List<McFlury> listMcFlurys) {
        this.listMcFlurys = listMcFlurys;
    }

    public List<Cafe> getListCafes() {
        return listCafes;
    }

    public void setListCafes(List<Cafe> listCafes) {
        this.listCafes = listCafes;
    }
}
package mcd.com.gt.mccomponents.objects.holders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import mcd.com.gt.mccomponents.activities.RecyclerActivity;
import mcd.com.gt.mccomponents.R;
import mcd.com.gt.mccomponents.objects.ejercicio2.McOrden;

/**
 * Created by aleksei.escobar on 02/11/2017.
 */

public class ItemOrderHolder extends RecyclerView.ViewHolder {

    private CardView cvContainer;
    private ImageView ivProduct;
    private TextView tvCon;
    private TextView tvSin;

    public ItemOrderHolder(View itemView) {
        super(itemView);

        cvContainer = itemView.findViewById(R.id.cv_container);
        ivProduct = itemView.findViewById(R.id.iv_product);
        tvCon = itemView.findViewById(R.id.tv_con);
        tvSin = itemView.findViewById(R.id.tv_sin);
    }

    public CardView getCvContainer() {
        return cvContainer;
    }

    public void setCvContainer(CardView cvContainer) {
        this.cvContainer = cvContainer;
    }

    public ImageView getIvProduct() {
        return ivProduct;
    }

    public void setIvProduct(ImageView ivProduct) {
        this.ivProduct = ivProduct;
    }

    public TextView getTvCon() {
        return tvCon;
    }

    public void setTvCon(TextView tvCon) {
        this.tvCon = tvCon;
    }

    public TextView getTvSin() {
        return tvSin;
    }

    public void setTvSin(TextView tvSin) {
        this.tvSin = tvSin;
    }

    public void setMcOrder(McOrden mcOrder){
        ivProduct.setImageResource(mcOrder.getFoodType().getImage());
        String strCon = "<b>Con: </b>";
        String strSin = "<b>Sin: </b>";
        for(String temp : mcOrder.getConList()){
            strCon += temp + ", ";
        }
        tvCon.setText(Html.fromHtml(strCon));

        for(String temp : mcOrder.getSinList()){
            strSin += temp + ", ";
        }
        tvSin.setText(Html.fromHtml(strSin));

    }
}

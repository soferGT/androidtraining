package mcd.com.gt.mccomponents.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mcd.com.gt.mccomponents.R;
import mcd.com.gt.mccomponents.objects.ejercicio2.FoodType;
import mcd.com.gt.mccomponents.objects.ejercicio2.McOrden;
import android.util.Log;




public class AddProductActivity extends AppCompatActivity  {
    private Spinner sTipo;
    private Switch switchAzucar;
    private Switch switchLeche;
    private Switch switchCebolla;
    private Switch switchPepinillos;
    private Switch switchOreo;
    private Switch switchM2M;
    private Switch switchTocino;
    private Switch switchChoco;
    private Switch switchCremora;

    private List<String> conList;
    private List<String> sinList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        getSupportActionBar().setTitle("Nuevo Producto");
        initComponents();
    }

    private void initComponents() {
        conList = new ArrayList<>();
        sinList = new ArrayList<>();
        sTipo = (Spinner) findViewById(R.id.sTipo);
        switchAzucar = (Switch)findViewById(R.id.sw_azucar);
        switchLeche = (Switch)findViewById(R.id.sw_leche);
        switchCebolla = (Switch) findViewById(R.id.sw_cebolla);
        switchPepinillos = (Switch) findViewById(R.id.sw_pepinillos);
        switchOreo = (Switch)findViewById(R.id.sw_tOreo);
        switchM2M=(Switch)findViewById(R.id.sw_m2m);
        switchChoco =(Switch) findViewById(R.id.sw_choco);
        switchTocino =(Switch) findViewById(R.id.sw_tocino);
        switchCremora = (Switch) findViewById(R.id.sw_crema);


        String[] options = getResources().getStringArray(R.array.orden_opt);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,options);
        sTipo.setAdapter(adapter);
        sTipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                switch (position){
                    case 0:
                        switchAzucar.setVisibility(View.VISIBLE);
                        switchLeche.setVisibility(View.VISIBLE);
                        switchCebolla.setVisibility(View.GONE);
                        switchPepinillos.setVisibility(View.GONE);
                        switchOreo.setVisibility(View.GONE);
                        switchM2M.setVisibility(View.GONE);
                        switchCremora.setVisibility(View.VISIBLE);
                        switchTocino.setVisibility(View.GONE);
                        switchChoco.setVisibility(View.GONE);
                        break;
                    case 1:
                        switchAzucar.setVisibility(View.GONE);
                        switchLeche.setVisibility(View.GONE);
                        switchCebolla.setVisibility(View.GONE);
                        switchPepinillos.setVisibility(View.GONE);
                        switchOreo.setVisibility(View.VISIBLE);
                        switchM2M.setVisibility(View.VISIBLE);
                        switchCremora.setVisibility(View.GONE);
                        switchTocino.setVisibility(View.GONE);
                        switchChoco.setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        switchCebolla.setVisibility(View.VISIBLE);
                        switchPepinillos.setVisibility(View.VISIBLE);
                        switchAzucar.setVisibility(View.GONE);
                        switchLeche.setVisibility(View.GONE);
                        switchOreo.setVisibility(View.GONE);
                        switchM2M.setVisibility(View.GONE);
                        switchCremora.setVisibility(View.GONE);
                        switchTocino.setVisibility(View.VISIBLE);
                        switchChoco.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void addProduct(View view){
        McOrden mcTemp = new McOrden();
        String text = sTipo.getSelectedItem().toString();
        switch (text) {
            case "McCafe":
                mcTemp.setFoodType(FoodType.CAFE);
                if(switchAzucar.isChecked()){
                    conList.add("Azucar");
                }else{
                    sinList.add("Azucar");
                }
                if(switchLeche.isChecked()){
                    conList.add("Leche Descremada");
                }else{
                    sinList.add("Leche Descremada");
                }
                if(switchCremora.isChecked()){
                    conList.add("Cremora");
                }else{
                    sinList.add("Cremora");
                }
                break;
            case "McFlurry":
                mcTemp.setFoodType(FoodType.MCFLURRY);
                if(switchOreo.isChecked())
                    conList.add("Topping Oreo");
                else
                    sinList.add("Topping Oreo");
                if(switchM2M.isChecked())
                    conList.add("M&M");
                else
                    sinList.add("M&M");
                if(switchChoco.isChecked()){
                    conList.add("Topping Chocolate");
                }else{
                    sinList.add("Topping Chocolate");
                }

                break;
            case "Hamburguesa":
                mcTemp.setFoodType(FoodType.HAMBURGUESA);
                if(switchCebolla.isChecked()){
                    conList.add("Cebolla");
                }else{
                    sinList.add("Cebolla");
                }
                if(switchPepinillos.isChecked())
                    conList.add("Pepinillos");
                else
                    sinList.add("Pepinillos");
                if(switchTocino.isChecked()){
                    conList.add("Tocino");
                }else{
                    sinList.add("Tocino");
                }
                break;
        }
        mcTemp.setConList(conList);
        mcTemp.setSinList(sinList);


        Intent intent = new Intent();
        intent.putExtra(RecyclerActivity.RESULT_STR,mcTemp);

        setResult(RESULT_OK,intent);
        finish();
    }
}

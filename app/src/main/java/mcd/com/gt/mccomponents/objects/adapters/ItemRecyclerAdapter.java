package mcd.com.gt.mccomponents.objects.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import mcd.com.gt.mccomponents.objects.ejercicio2.McOrden;
import mcd.com.gt.mccomponents.objects.holders.ItemOrderHolder;

/**
 * Created by aleksei.escobar on 14/11/2017.
 */

public class ItemRecyclerAdapter extends RecyclerView.Adapter<ItemOrderHolder> {

    private List<McOrden> mcOrdenList;
    private Activity activity;
    private int resourceLayout;

    public ItemRecyclerAdapter(List<McOrden> mcOrdenList, Activity activity, int resourceLayout) {
        this.mcOrdenList = mcOrdenList;
        this.activity = activity;
        this.resourceLayout = resourceLayout;
    }

    @Override
    public ItemOrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemOrderHolder(LayoutInflater.from(activity).inflate(resourceLayout,parent,false));
    }

    @Override
    public void onBindViewHolder(ItemOrderHolder holder, int position) {
        McOrden mcOrden = mcOrdenList.get(position);
        holder.setMcOrder(mcOrden);

        holder.getCvContainer().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //abrir activity con detalle de la orden
            }
        });
    }

    @Override
    public int getItemCount() {
        return mcOrdenList.size();
    }
}

package mcd.com.gt.mccomponents.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

import mcd.com.gt.mccomponents.R;
import mcd.com.gt.mccomponents.objects.ejercicio1.Cafe;
import mcd.com.gt.mccomponents.objects.ejercicio1.Hamburguesa;
import mcd.com.gt.mccomponents.objects.ejercicio1.McFlury;
import mcd.com.gt.mccomponents.objects.ejercicio1.NuevaOrden;

public class EjercicioParcelableActivity extends AppCompatActivity {
    private TextView tvNuevaOrden;
    private NuevaOrden nuevaOrden;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio_parcelable);
        initComponents();
    }

    private void initComponents(){
        nuevaOrden = getIntent().getParcelableExtra(
                OptionActivity.NUEVA_ORDEN);
        tvNuevaOrden = (TextView) findViewById(R.id.tv_nueva_orden);
        List<Hamburguesa> hamburguesaList = nuevaOrden.getListHamburguesas();
        List<Cafe> cafeList = nuevaOrden.getListCafes();
        List<McFlury> mcFluryList = nuevaOrden.getListMcFlurys();

        String misPedidos="";

        for(Hamburguesa hamburguesa:hamburguesaList){
            misPedidos += "\n     Pepinillos: " + getVal(hamburguesa.isPepinillos());
            misPedidos += "\n     Tomates: " + getVal(hamburguesa.isTomate());
            misPedidos += "\n     Lechuga: " + getVal(hamburguesa.isLechuga());
            misPedidos += "\n     Cebolla: " + getVal(hamburguesa.isCebolla());
            misPedidos += "\n\n";
        }
        misPedidos += "Cafecitos ";
        for(Cafe cafe: cafeList){
            misPedidos += "\n     Azucar: "+ getVal(cafe.isAzucar());
            misPedidos += "\n     Cafeina: "+ getVal(cafe.isCafeina());
            misPedidos += "\n     Leche: " + getVal(cafe.isLeche());
            misPedidos += "\n\n";
        }
        misPedidos += "McFlury's ";

        for(McFlury mcFlury:mcFluryList){
            misPedidos += "\n     Toping: "+ getVal(mcFlury.isToping());
            misPedidos += "\n     Oreo: "+ getVal(mcFlury.isOreo());
            misPedidos += "\n     M&M: " + getVal(mcFlury.isM_m());
            misPedidos += "\n\n";
        }

        tvNuevaOrden.setText(misPedidos);
    }

    private String getVal(boolean bool){
        return((bool)? "Si": "No");
    }

}

package mcd.com.gt.mccomponents.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import mcd.com.gt.mccomponents.R;
import mcd.com.gt.mccomponents.SplashScreenActivity;
import mcd.com.gt.mccomponents.objects.ejercicio1.Cafe;
import mcd.com.gt.mccomponents.objects.ejercicio1.Hamburguesa;
import mcd.com.gt.mccomponents.objects.ejercicio1.McFlury;
import mcd.com.gt.mccomponents.objects.ejercicio1.NuevaOrden;
import mcd.com.gt.mccomponents.objects.Orden;

public class OptionActivity extends AppCompatActivity {
    public static final String NUEVA_ORDEN = "llave";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_option);
        Orden orden = getIntent().getParcelableExtra(SplashScreenActivity.KEY);
        Toast.makeText(this,"Se pidio: " + orden.getCanCafe() + "Cafes " + orden.getCanHamburguesa() + "Hamburguesas" + orden.getCanMacFlury() + "McFlury " + orden.getCanPapitas() + "Papas ",
        Toast.LENGTH_LONG).show();
    }

    public void ejercicioParcelable(View view){
        List<Cafe> cafeList = new ArrayList<Cafe>();
        List<Hamburguesa> hamburguesaList = new ArrayList<Hamburguesa>();
        List<McFlury> mcFlurryList = new ArrayList<McFlury>();
        Hamburguesa hamburguesa1 = new Hamburguesa(false,true,true,true);
        Hamburguesa hamburguesa2 = new Hamburguesa(true,false, false, false);
        hamburguesaList.add(hamburguesa1);
        hamburguesaList.add(hamburguesa2);

        Cafe cafe1 = new Cafe(true, false, true);
        Cafe cafe2 = new Cafe(false, true, true);
        cafeList.add(cafe1);
        cafeList.add(cafe2);

        McFlury mcFlury1 = new McFlury(false, true,false);
        McFlury mcFlury2 = new McFlury(false, false,true);

        mcFlurryList.add(mcFlury1);
        mcFlurryList.add(mcFlury2);

        Intent intent = new Intent(this, EjercicioParcelableActivity.class);


        NuevaOrden nuevaOrden = new NuevaOrden(hamburguesaList,mcFlurryList,cafeList);
        intent.putExtra(NUEVA_ORDEN,nuevaOrden);

        startActivity(intent);


    }
    public void goToCicle(View view){
        startActivity(new Intent(this,CicleActivity.class));
    }

    public void goToCbx(View view){
        startActivity(new Intent(this,ChekBoxActivity.class));
    }

    public void goToLv(View view){
        startActivity(new Intent(this,ListViewActivity.class));
    }

    public void goToResult(View view){
        startActivity(new Intent(this, ResultActivity.class));
    }

    public void goToRecycler (View view){startActivity(new Intent(this,RecyclerActivity.class));}

}

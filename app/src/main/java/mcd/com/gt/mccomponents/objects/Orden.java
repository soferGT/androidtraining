package mcd.com.gt.mccomponents.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aleksei.escobar on 26/09/2017.
 */
public class Orden implements Parcelable {
    private int canHamburguesa;
    private int canMacFlury;
    private int canPapitas;
    private int canCafe;

    public Orden(){

    }

    public Orden(int canHamburguesa, int canMacFlury, int canPapitas, int canCafe) {
        this.canHamburguesa = canHamburguesa;
        this.canMacFlury = canMacFlury;
        this.canPapitas = canPapitas;
        this.canCafe = canCafe;
    }

    protected Orden(Parcel in) {
        canHamburguesa = in.readInt();
        canMacFlury = in.readInt();
        canPapitas = in.readInt();
        canCafe = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(canHamburguesa);
        dest.writeInt(canMacFlury);
        dest.writeInt(canPapitas);
        dest.writeInt(canCafe);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Orden> CREATOR = new Parcelable.Creator<Orden>() {
        @Override
        public Orden createFromParcel(Parcel in) {
            return new Orden(in);
        }

        @Override
        public Orden[] newArray(int size) {
            return new Orden[size];
        }
    };

    public int getCanHamburguesa() {
        return canHamburguesa;
    }

    public void setCanHamburguesa(int canHamburguesa) {
        this.canHamburguesa = canHamburguesa;
    }

    public int getCanMacFlury() {
        return canMacFlury;
    }

    public void setCanMacFlury(int canMacFlury) {
        this.canMacFlury = canMacFlury;
    }

    public int getCanPapitas() {
        return canPapitas;
    }

    public void setCanPapitas(int canPapitas) {
        this.canPapitas = canPapitas;
    }

    public int getCanCafe() {
        return canCafe;
    }

    public void setCanCafe(int canCafe) {
        this.canCafe = canCafe;
    }
}
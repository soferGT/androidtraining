package mcd.com.gt.mccomponents.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mcd.com.gt.mccomponents.R;
import mcd.com.gt.mccomponents.objects.adapters.ItemRecyclerAdapter;
import mcd.com.gt.mccomponents.objects.ejercicio2.FoodType;
import mcd.com.gt.mccomponents.objects.ejercicio2.ListMcOrdenSave;
import mcd.com.gt.mccomponents.objects.ejercicio2.McOrden;

public class RecyclerActivity extends AppCompatActivity {
    private final int NEW_PRODUCT_CODE = 321;
    public static final String RESULT_STR = "strResult";
    public static final String SAVE_STATE = "listSave";
    private RecyclerView rvListOrder;
    private ItemRecyclerAdapter itemRecyclerAdapter;
    List<McOrden> result = new ArrayList<McOrden>();
    //ArrayList<McOrden> listSave = new ArrayList<>();
    ListMcOrdenSave listSave = new ListMcOrdenSave();


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        listSave.setMcOrdenList(result);
        Log.d("TMC","onSaveInstanceState "+listSave.getMcOrdenList().size());
        outState.putParcelable(SAVE_STATE, listSave);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("TMC","onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Agregar Producto");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(RecyclerActivity.this, AddMacOrderActivity.class), NEW_PRODUCT_CODE);
            }
        });
        //Recuperar el estado de la lista de productos
        if (savedInstanceState != null){
            listSave = savedInstanceState.getParcelable(SAVE_STATE);
            result = listSave.getMcOrdenList();
        }
        initComponents();
    }

    private void initComponents(){
        rvListOrder = findViewById(R.id.rv_list_order);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,1);
        rvListOrder.setLayoutManager(gridLayoutManager);
        itemRecyclerAdapter = new ItemRecyclerAdapter(result,this,
                R.layout.item_line_order);
        rvListOrder.setAdapter(itemRecyclerAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if((requestCode == NEW_PRODUCT_CODE)&& (resultCode == RESULT_OK)&& (data != null)){
            result.add((McOrden)data.getParcelableExtra(RESULT_STR));
            itemRecyclerAdapter.notifyItemInserted(result.size() - 1);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d("TMC","onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);


    }
}

package mcd.com.gt.mccomponents.objects.ejercicio1;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aleksei.escobar on 10/10/2017.
 */

public class Hamburguesa implements Parcelable{
    private boolean pepinillos;
    private boolean cebolla;
    private boolean lechuga;
    private boolean tomate;

    public Hamburguesa() {
    }

    public Hamburguesa(boolean pepinillos, boolean cebolla, boolean lechuga, boolean tomate) {
        this.pepinillos = pepinillos;
        this.cebolla = cebolla;
        this.lechuga = lechuga;
        this.tomate = tomate;
    }

    protected Hamburguesa(Parcel in) {
        pepinillos = in.readByte() != 0x00;
        cebolla = in.readByte() != 0x00;
        lechuga = in.readByte() != 0x00;
        tomate = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (pepinillos ? 0x01 : 0x00));
        dest.writeByte((byte) (cebolla ? 0x01 : 0x00));
        dest.writeByte((byte) (lechuga ? 0x01 : 0x00));
        dest.writeByte((byte) (tomate ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Hamburguesa> CREATOR = new Parcelable.Creator<Hamburguesa>() {
        @Override
        public Hamburguesa createFromParcel(Parcel in) {
            return new Hamburguesa(in);
        }

        @Override
        public Hamburguesa[] newArray(int size) {
            return new Hamburguesa[size];
        }
    };

    public boolean isPepinillos() {
        return pepinillos;
    }

    public void setPepinillos(boolean pepinillos) {
        this.pepinillos = pepinillos;
    }

    public boolean isCebolla() {
        return cebolla;
    }

    public void setCebolla(boolean cebolla) {
        this.cebolla = cebolla;
    }

    public boolean isLechuga() {
        return lechuga;
    }

    public void setLechuga(boolean lechuga) {
        this.lechuga = lechuga;
    }

    public boolean isTomate() {
        return tomate;
    }

    public void setTomate(boolean tomate) {
        this.tomate = tomate;
    }
}
package mcd.com.gt.mccomponents;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import mcd.com.gt.mccomponents.activities.OptionActivity;
import mcd.com.gt.mccomponents.objects.Orden;

public class SplashScreenActivity extends AppCompatActivity {

    private Handler mHandler;
    public static final String KEY = "llave";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mHandler = new Handler();
        mHandler.postDelayed(runOption, 5_000);
    }

    private Runnable runOption = new Runnable() {
        @Override
        public void run() {
            goToOptions();
        }
    };

    private void goToOptions(){
        Intent intent = new Intent(this, OptionActivity.class);
        Orden orden = new Orden(3,0,3,0);
        intent.putExtra(KEY,orden);

        startActivity(intent);
    }
}

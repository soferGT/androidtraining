package mcd.com.gt.mccomponents.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.view.MenuItem;
import java.util.ArrayList;
import java.util.List;

import mcd.com.gt.mccomponents.R;
import mcd.com.gt.mccomponents.objects.adapters.MacOrderAdapter;
import mcd.com.gt.mccomponents.objects.ejercicio2.FoodType;
import mcd.com.gt.mccomponents.objects.ejercicio2.MacOption;
import mcd.com.gt.mccomponents.objects.ejercicio2.McOrden;

public class AddMacOrderActivity extends AppCompatActivity {

    private ImageView ivSelected;
    private Spinner sMacOrder;
    private RecyclerView rvOptions;
    private MacOrderAdapter macOrderAdapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.add_mac_order_menu,menu);
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mac_order);
        initComponents();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.add_mac_order:
                McOrden macOrden = new McOrden();
                macOrden.setFoodType(FoodType.getFoodType(sMacOrder.getSelectedItemPosition()));
                for(MacOption macOption:macOrderAdapter.getMacOptions()){
                    if(macOption.isSelected()){
                        macOrden.getConList().add(macOption.getStrOption());
                    }else{
                        macOrden.getSinList().add(macOption.getStrOption());
                    }
                }
                Intent resultIntent = new Intent();
                resultIntent.putExtra(RecyclerActivity.RESULT_STR,macOrden);
                setResult(RESULT_OK,resultIntent);
                break;
        }
        finish();
        return super.onOptionsItemSelected(item);
    }

    private void initComponents(){
        ivSelected = findViewById(R.id.iv_selected);
        sMacOrder = findViewById(R.id.s_mac_order);
        rvOptions = findViewById(R.id.rv_order_options);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,3);
        rvOptions.setLayoutManager(gridLayoutManager);

        sMacOrder.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,getResources().
                getStringArray(R.array.orden_opt)));

        sMacOrder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                changeOptions();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        changeOptions();
    }

    private void changeOptions(){
        switch (sMacOrder.getSelectedItemPosition()){
            case 0:
                ivSelected.setImageResource(R.drawable.cafe);
                macOrderAdapter = new MacOrderAdapter(
                        generateMacOpt(getResources().getStringArray(R.array.cafe_options)),this);
                break;
            case 1:
                ivSelected.setImageResource(R.drawable.mcflury);
                macOrderAdapter = new MacOrderAdapter(
                        generateMacOpt(getResources().getStringArray(R.array.mcflury_options)),this);

                break;
            case 2:
                ivSelected.setImageResource(R.drawable.hamburguesa);
                macOrderAdapter = new MacOrderAdapter(
                        generateMacOpt(getResources().getStringArray(R.array.hamburguesa_options)),this);
                break;
        }

        rvOptions.setAdapter(macOrderAdapter);
    }

    private List<MacOption> generateMacOpt(String[] macOpt){
        List<MacOption> result = new ArrayList<>();

        for (String strOpt : macOpt){
            result.add(new MacOption(strOpt,false));
        }

        return  result;
    }

}

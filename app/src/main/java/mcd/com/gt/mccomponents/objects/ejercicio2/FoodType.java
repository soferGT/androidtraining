package mcd.com.gt.mccomponents.objects.ejercicio2;

import android.content.Context;

import mcd.com.gt.mccomponents.R;

/**
 * Created by aleksei.escobar on 02/11/2017.
 */

public enum FoodType {
    CAFE,
    MCFLURRY,
    HAMBURGUESA;

    public String toString(Context context){

        switch (this){
            case CAFE:
                return context.getResources().getString(R.string.opt_cafe);
            case MCFLURRY:
                return context.getResources().getString(R.string.opt_mcflurry);
            case HAMBURGUESA:
                return context.getResources().getString(R.string.opt_hamburguer);
                default:
                    return "";
        }
    }

    public int getImage(){
        switch (this){
            case CAFE:
                return R.drawable.cafe;
            case MCFLURRY:
                return R.drawable.mcflury;
            case HAMBURGUESA:
                return R.drawable.hamburguesa;
                default:
                   return R.drawable.box;
        }
    }

    public static FoodType getFoodType(int position){
        switch (position){
            case 0:
                return CAFE;
            case 1:
                return MCFLURRY;
            case 2:
                return HAMBURGUESA;
                default:
                    return null;
        }
    }
}

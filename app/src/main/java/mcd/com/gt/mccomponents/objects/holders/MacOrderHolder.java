package mcd.com.gt.mccomponents.objects.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;

import mcd.com.gt.mccomponents.R;
import mcd.com.gt.mccomponents.objects.ejercicio2.MacOption;

/**
 * Created by aleksei.escobar on 23/11/2017.
 */

public class MacOrderHolder extends RecyclerView.ViewHolder {
    private CheckBox cbxOption;

    public MacOrderHolder(View itemView) {
        super(itemView);
        this.cbxOption = itemView.findViewById(R.id.cbx_option);
    }

    public CheckBox getCbxOption() {
        return cbxOption;
    }

    public void setCbxOption(CheckBox cbxOption) {
        this.cbxOption = cbxOption;
    }

    public void setMacOption(MacOption macOption){
        cbxOption.setText(macOption.getStrOption());
    }
}

package mcd.com.gt.mccomponents.objects.ejercicio1;

/**
 * Created by aleksei.escobar on 10/10/2017.
 */
import android.os.Parcel;
import android.os.Parcelable;

public class Cafe implements Parcelable {
    private boolean cafeina;
    private boolean leche;
    private boolean azucar;

    public Cafe() {
    }

    public Cafe(boolean cafeina, boolean leche, boolean azucar) {
        this.cafeina = cafeina;
        this.leche = leche;
        this.azucar = azucar;
    }

    protected Cafe(Parcel in) {
        cafeina = in.readByte() != 0x00;
        leche = in.readByte() != 0x00;
        azucar = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (cafeina ? 0x01 : 0x00));
        dest.writeByte((byte) (leche ? 0x01 : 0x00));
        dest.writeByte((byte) (azucar ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Cafe> CREATOR = new Parcelable.Creator<Cafe>() {
        @Override
        public Cafe createFromParcel(Parcel in) {
            return new Cafe(in);
        }

        @Override
        public Cafe[] newArray(int size) {
            return new Cafe[size];
        }
    };

    public boolean isCafeina() {
        return cafeina;
    }

    public void setCafeina(boolean cafeina) {
        this.cafeina = cafeina;
    }

    public boolean isLeche() {
        return leche;
    }

    public void setLeche(boolean leche) {
        this.leche = leche;
    }

    public boolean isAzucar() {
        return azucar;
    }

    public void setAzucar(boolean azucar) {
        this.azucar = azucar;
    }
}
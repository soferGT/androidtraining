package mcd.com.gt.mccomponents.objects.ejercicio1;

/**
 * Created by aleksei.escobar on 10/10/2017.
 */
import android.os.Parcel;
import android.os.Parcelable;

public class McFlury implements Parcelable {
    private boolean toping;
    private boolean oreo;
    private boolean m_m;

    public McFlury() {
    }

    public McFlury(boolean toping, boolean oreo, boolean m_m) {
        this.toping = toping;
        this.oreo = oreo;
        this.m_m = m_m;
    }

    protected McFlury(Parcel in) {
        toping = in.readByte() != 0x00;
        oreo = in.readByte() != 0x00;
        m_m = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (toping ? 0x01 : 0x00));
        dest.writeByte((byte) (oreo ? 0x01 : 0x00));
        dest.writeByte((byte) (m_m ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<McFlury> CREATOR = new Parcelable.Creator<McFlury>() {
        @Override
        public McFlury createFromParcel(Parcel in) {
            return new McFlury(in);
        }

        @Override
        public McFlury[] newArray(int size) {
            return new McFlury[size];
        }
    };

    public boolean isToping() {
        return toping;
    }

    public void setToping(boolean toping) {
        this.toping = toping;
    }

    public boolean isOreo() {
        return oreo;
    }

    public void setOreo(boolean oreo) {
        this.oreo = oreo;
    }

    public boolean isM_m() {
        return m_m;
    }

    public void setM_m(boolean m_m) {
        this.m_m = m_m;
    }
}

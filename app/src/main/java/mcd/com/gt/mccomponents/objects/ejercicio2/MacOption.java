package mcd.com.gt.mccomponents.objects.ejercicio2;

import java.util.ArrayList;

/**
 * Created by aleksei.escobar on 23/11/2017.
 */

public class MacOption  {
    private String strOption;
    private boolean selected;

    public MacOption(){


    }

    public MacOption(String strOption, boolean selected) {
        this.strOption = strOption;
        this.selected = selected;
    }

    public String getStrOption() {
        return strOption;
    }

    public void setStrOption(String strOption) {
        this.strOption = strOption;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}

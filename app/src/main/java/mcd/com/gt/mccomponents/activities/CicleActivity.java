package mcd.com.gt.mccomponents.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import mcd.com.gt.mccomponents.R;

public class CicleActivity extends AppCompatActivity {

    private Spinner sOptions;
    private ImageView ivSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cicle);
        if (savedInstanceState != null) {
            Log.i("Saved state", ": " + savedInstanceState.getInt("selectedImg"));
        }
        initComponents();
    }

    private void initComponents(){
        ivSelected = (ImageView) findViewById(R.id.iv_selected_image);
        sOptions = (Spinner) findViewById(R.id.s_options);

        String[] options = getResources().getStringArray(R.array.img_opt);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,options);
        sOptions.setAdapter(adapter);
        sOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                switch (position){
                    case 0:
                        ivSelected.setImageResource(R.drawable.atm);
                        break;
                    case 1:
                        ivSelected.setImageResource(R.drawable.bag);
                        break;
                    case 2:
                        ivSelected.setImageResource(R.drawable.basket);
                        break;
                    case 3:
                        ivSelected.setImageResource(R.drawable.box);
                        break;
                    case 4:
                        ivSelected.setImageResource(R.drawable.briefcase);
                        break;
                    case 5:
                        ivSelected.setImageResource(R.drawable.calculator);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle state){
        super.onSaveInstanceState(state);
        Log.i("CicleActivity","onSaveInstanceState");

        state.putInt("selectedImg",sOptions.getSelectedItemPosition());
    }

    @Override
    protected void onRestoreInstanceState(Bundle saveInstanceState) {
        super.onRestoreInstanceState(saveInstanceState);
        Log.i("CicleActivity", "onRestoreInstanceState");
        if (saveInstanceState != null){
            Log.i("Saved state", ": " + saveInstanceState.getInt("selectedImg"));
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.i("CicleActivity","onStart");
    }
    @Override
    protected void onResume(){
        super.onResume();
        Log.i("CicleActivity","onResume");
    }
    @Override
    protected void onRestart(){
        super.onRestart();
        Log.i("CicleActivity","onRestart");
    }
    @Override
    protected void onStop(){
        super.onStop();
        Log.i("CicleActivity","onStop");
    }
    @Override
    protected void onPause(){
        super.onPause();
        Log.i("CicleActivity","onPause");
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.i("CicleActivity","onDestroy");
    }
}
